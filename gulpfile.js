var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function(){
    gulp.src('./HTML_CSS/style.sass')
        .pipe(sass())
        .pipe(gulp.dest('./HTML_CSS/'));
});

gulp.task('watch', function(){
    gulp.watch('./HTML_CSS/*.sass', ['sass']); 
});

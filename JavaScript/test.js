var assert = require('assert');
var main = require('./main.js');

describe('Get Products', function () {
    it('Should return products list', function () {
        var products = main.getProduct({
            1: 1,
            2: false,
            3: 0
        });
        if (products == undefined) {
            throw new Error('Product list is not defined');
        } else if (products.length == 0) {
            throw new Error('Product list is empty');
        }
    });
    it('Should return Student Account', function () {
        var products = main.getProduct({
            1: 2,
            2: true,
            3: 0
        });
        var studentAccount = false;
        for (var i = 0; products.length > i; i++) {
            if (products[i]['Title'] == 'Student Account') {
                studentAccount = true;
            }
        }
        if (!studentAccount) {
            throw new Error('Returned wrong account type');
        }
    });
});

describe('Get bundles', function () {
    it('Should return recommended bundle', function () {
        var bundle = main.getRecommendedBundle({
            1: 1,
            2: false,
            3: 0
        });

        if (bundle == undefined) {
            throw new Error('No featured bundle was returned');
        }
    });
    it('Should return Gold bundle', function () {
        var bundle = main.getRecommendedBundle({
            1: 2,
            2: false,
            3: 3
        });

        if (bundle['Title'] !== 'Gold') {
            throw new Error('Returned wrong or did not return any bundle. Returned "' + bundle['Title'] + '"');
        }

    });
});

describe('Modify bundle', function () {
    it('Should remove a product and return another bundle', function(){
        main.modifyBundle(4, {
            1: 2,
            2: false,
            3: 3
        }, {
            6: false
        }, {
            error: function(text) {
                throw new Error(text);
            },
            success: function(text, bundle) {
                console.log(text);
            }
        });
    })
});

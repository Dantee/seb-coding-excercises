var Products = [
    {
        ID: 1,
        Title: 'Current Account',
        Rules: [
            {
                ID: 3,
                Value: [1, 2, 3]
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ],
        Type: 'Account'
    },
    {
        ID: 2,
        Title: 'Current Account Plus',
        Rules: [
            {
                ID: 3,
                Value: [3]
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ],
        Type: 'Account'
    },
    {
        ID: 3,
        Title: 'Junior Saver Account',
        Rules: [
            {
                ID: 1,
                Value: [1]
            }
        ],
        Type: 'Account'
    },
    {
        ID: 4,
        Title: 'Student Account',
        Rules: [
            {
                ID: 2,
                Value: true
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ],
        Type: 'Account'
    },
    {
        ID: 5,
        Title: 'Debit Card',
        Rules: [
            {
                ID: 1,
                Value: [2, 3]
            }
        ]
    },
    {
        ID: 6,
        Title: 'Credit Card',
        Rules: [
            {
                ID: 3,
                Value: [2, 3]
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ]
    },
    {
        ID: 7,
        Title: 'Gold Credit Card',
        Rules: [
            {
                ID: 3,
                Value: [3]
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ]
    }
];

module.exports = Products;
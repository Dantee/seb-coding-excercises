var Questions = [
    {
        ID: 1,
        Question: 'Age?',
        Answers: [
            {
                Title: '0-17',
                Value: 1
            },
            {
                Title: '18-64',
                Value: 2
            },
            {
                Title: '65+',
                Value: 3
            }
        ]
    },
    {
        ID: 2,
        Question: 'Student?',
        Answers: [
            {
                Title: 'Yes',
                Value: true
            },
            {
                Title: 'No',
                Value: false
            }
        ]
    },
    {
        ID: 3,
        Question: 'Income?',
        Answers: [
            {
                Title: 0,
                Value: 0
            },
            {
                Title: '1-12000',
                Value: 1
            },
            {
                Title: '12001-40000',
                Value: 2
            },
            {
                Title: '40000+',
                Value: 3
            }
        ]
    }
];

module.exports = Questions;
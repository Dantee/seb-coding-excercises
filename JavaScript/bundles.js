var Bundles = [
    {
        ID: 1,
        Title: 'Junior saver',
        Rules: [
            {
                ID: 1,
                Value: [1]
            }
        ],
        Value: 0,
        Products: [3]
    },
    {
        ID: 2,
        Title: 'Student',
        Rules: [
            {
                ID: 1,
                Value: [2, 3]
            },
            {
                ID: 2,
                Value: true
            }
        ],
        Value: 0,
        Products: [4, 5, 6]
    },
    {
        ID: 3,
        Title: 'Classic',
        Rules: [
            {
                ID: 1,
                Value: [2, 3]
            },
            {
                ID: 3,
                Value: [1, 2, 3]
            }
        ],
        Value: 1,
        Products: [1, 5]
    },
    {
        ID: 4,
        Title: 'Classic Plus',
        Rules: [
            {
                ID: 3,
                Value: [2, 3]
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ],
        Value: 2,
        Products: [1, 5, 6]
    },
    {
        ID: 5,
        Title: 'Gold',
        Rules: [
            {
                ID: 3,
                Value: [3]
            },
            {
                ID: 1,
                Value: [2, 3]
            }
        ],
        Value: 3,
        Products: [2, 5, 7]
    }
];

module.exports = Bundles;
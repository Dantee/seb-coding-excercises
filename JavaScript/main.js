var Questions = require('./questions');
var Products = require('./products');
var Bundles = require('./bundles');


//Array comparison prototype.

Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return

    if (!array)
        return false;

    // compare lengths - can save a lot of time

    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        // Check if we have nested arrays

        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays

            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}

            return false;
        }
    }
    return true;
};

var compareRules = function (rules, answers) {
    var correct = true;

    if (rules == null) {
        rules = [];
    }

    if (answers == null) {
        answers = {};

        //Return false if no answers were given.

        correct = false;
    }

    for (var i = 0; rules.length > i; i++) {

        var ruleID = rules[i]['ID'],
            ruleValue = rules[i]['Value'];

        //Checks if rules are fitting by type
        
        if (answers[ruleID] !== undefined) {
            if (ruleValue.constructor === Array) {
                if (ruleValue.indexOf(answers[ruleID]) == -1) {
                    correct = false;
                }
            } else if (typeof ruleValue === 'boolean') {
                if (ruleValue !== answers[ruleID]) {
                    correct = false;
                }
            }
        }
    }

    return correct;
};

var getProduct = function (answers) {
    var prod = [];

    for (var i = 0; Products.length > i; i++) {
        var thisProd = Products[i],
            rules = thisProd['Rules'];

        //Check if product rules fits answers and return array of products.

        if (compareRules(rules, answers)) {
            prod.push(thisProd);
        }
    }

    return prod;
};

var getBundles = function (answers) {
    var bundles = [];

    for (var i = 0; Bundles.length > i; i++) {
        var thisBundle = Bundles[i],
            rules = thisBundle['Rules'];

        //Check if bundle rules fits answers and return array of bundles.

        if (compareRules(rules, answers)) {
            bundles.push(thisBundle);
        }
    }

    return bundles;
};

var getRecommendedBundle = function (answers) {
    if (answers == null) return false;

    var recommendedBundle,
        bundleValue = 0,
        bundleList = getBundles(answers);

    for (var i = 0; bundleList.length > i; i++) {

        //Check if next bundle value is higher than previous and set as featured.

        if (bundleValue <= bundleList[i]['Value']) {
            recommendedBundle = bundleList[i];
            bundleValue = bundleList[i]['Value'];
        }
    }

    return recommendedBundle;
};

var returnBundleById = function (id) {
    var bundle = false;

    //Returns bundle by ID given from the list, returns false if not existing.

    for (var i = 0; Bundles.length > i; i++) {
        if (Bundles[i]['ID'] == id) {
            bundle = Bundles[i];
        }
    }
    return bundle;
};

var modifyBundle = function (bundleId, answers, modifications, cb) {
    var bundle = returnBundleById(bundleId);

    //Bundle ID was not existing.

    if (bundle == false) {
        return cb.error('Bundle does not exist.');
    }

    //Bundle and the answers for it are different

    if (!compareRules(bundle['Rules'], answers)) {
        return cb.error('Bundle does not fit with the answers');
    }

    //If none of modifications was given, return bundle by ID it was given.

    if (modifications == null) {
        return bundle;
    }

    //Get modification ids.

    var modIds = Object.keys(modifications),
        bundleProducts = bundle['Products'].slice();

    //Go through modifications and add or remove products

    (function () {
        for (var i = 0; modIds.length > i; i++) {
            if (modifications[modIds[i]]) {

                //If modification is true, add product to bundle

                bundleProducts.push(parseInt(modIds[i]));
            } else {

                //If modification is false and it does exist, remove product.

                var modIndex = bundle['Products'].indexOf(parseInt(modIds[i]));
                if (modIndex !== -1) {
                    bundleProducts.splice(modIndex, 1);
                }
            }
        }
    })();

    //Check for bundle which fits modifications.

    var newBundle = bundle;
    (function () {
        for (var i = 0; Bundles.length > i; i++) {
            var products = Bundles[i]['Products'];

            //If it has same products after modifications, set new bundle.

            if (products.equals(bundleProducts)) {
                newBundle = Bundles[i]
            }

        }
    })();

    //Check if new bundle fits the answers

    if (!compareRules(newBundle['Rules'], answers)) {
        return cb.error('We found new bundle, but it does not fit your answers');
    }

    //Check if new bundle is not the same as bundle was given.

    if (newBundle['Title'] == bundle['Title']) {
        return cb.error('Could not find bundle by your modifications.');
    } else {
        return cb.success('Your new bundle is ' + newBundle['Title'], newBundle);
    }
};


module.exports = {
    getProduct: getProduct,
    getBundles: getBundles,
    getRecommendedBundle: getRecommendedBundle,
    modifyBundle: modifyBundle
};